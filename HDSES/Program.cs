﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Google.Protobuf;

namespace HDSES
{
    public class ERRORCODE
    {
        static int BASECODE = 0;
        static public int ERROR_SUCCESS = 0;
        static public int ERROR_HDDINDEX = BASECODE + 1;
        static public int ERROR_LESSHDDINDEX = BASECODE + 2;
        static public int ERROR_CONFIG = BASECODE + 3;
        static public int ERROR_SEQUENCE = BASECODE + 4;
        static public int ERROR_CONFIGNAME = BASECODE + 5;
        static public int ERROR_EXENOEXIST = BASECODE + 7;
    }
    class Program
    {
        static String _TAG = "HDSES";
        static void logIt(String msg)
        {
	        String ss = String.Format("[0]{1}", _TAG, msg);
	        Trace.WriteLine(ss);
	        Console.WriteLine(ss);
        }
        static Configs ReserializeMethod()
        {
            //反序列化 
            Configs ms = null;
            String sPathdata = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "hdsesconfig.dat");
            if(File.Exists(sPathdata))
            {
                Mutex lockdata = new Mutex(false, Path.GetFileName(sPathdata));
                global::ProfileConfig pfconf;
                lockdata.WaitOne();
                try
                {
                    using (var input = File.OpenRead(sPathdata))
                    {
                        pfconf = global::ProfileConfig.Parser.ParseFrom(input);
                    }
                }finally
                {
                    lockdata.ReleaseMutex();
                }
                ms = new Configs();
                foreach(var p in pfconf.Profile)
                {
                    Profile pf = new Profile();
                    pf.Name = p.Name;
                    pf.Pass = p.Pass==0?1:p.Pass;
                    pf.Descript = p.Description;
                    pf.SupportMethod = p.Orgsupport;
                    pf.WriteData = p.Bytes;
                    ms.profiles.Items.Add(pf);
                }
                return ms;
            }
            

            String sPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "hdsesconfig.xml");
            if(!File.Exists(sPath))
            {
                logIt(String.Format("File {0} is not exist.", sPath));
                return ms;
            }
            bool bCreateNew;
            Mutex lockxml = new Mutex(false, Path.GetFileName(sPath), out bCreateNew);
            //if (bCreateNew)
            {
                lockxml.WaitOne();
                try
                {
                    using (FileStream fs = new FileStream(sPath, FileMode.Open))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(Configs));
                        ms = xs.Deserialize(fs) as Configs;

                    }
                }
                finally
                {
                    lockxml.ReleaseMutex();
                }
            }
            return ms;
        }


        static void Usage()
        {
            Console.WriteLine("-label=x ");
            Console.WriteLine("-index=x  hdd index.");
            Console.WriteLine("-pass=x default 1, do repeat times.");
            Console.WriteLine("-name=abc use config one of namelist.");
            Console.WriteLine("");

        }

        static void CheckProcess(int processId)
        {
            try
            {
                Process process = Process.GetProcessById(processId);
                process.WaitForExit();
                Console.WriteLine("Process [{0}] exited.", processId);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Process [{0}] not running.", processId);
            }

            waithand.Set();
        }

        static void SecurWipe(int index)
        {
            ProcessInterface processInterace = new ProcessInterface();
            processInterace.OnProcessOutput += new ProcessEventHanlder(processInterace_OnProcessOutput);
            processInterace.OnProcessError += new ProcessEventHanlder(processInterace_OnProcessError);
            processInterace.OnProcessInput += new ProcessEventHanlder(processInterace_OnProcessInput);
            processInterace.OnProcessExit += new ProcessEventHanlder(processInterace_OnProcessExit);
string sCmdline = String.Format(@"select disk {0}
clean all
exit", index);
            string filename = Path.GetTempFileName();
            File.AppendAllText(filename, sCmdline);
            sCmdline = String.Format(" /s \"{0}\"", filename);
            processInterace.StartProcess("DiskPart.exe ", sCmdline);

            while(processInterace.IsProcessRunning)
            {
                Thread.Sleep(5000);
            }
            try
            {
                File.Delete(filename);
            }catch(Exception)
            { }
            return;
        }
        

        static EventWaitHandle waithand = new EventWaitHandle(false, EventResetMode.ManualReset);
        static EventWaitHandle hangflag = new EventWaitHandle(false, EventResetMode.AutoReset);
        static int Main(string[] args)
        {

            //Profile pofile = new Profile();
           // ProfileConfig pofilelist = new ProfileConfig();
           // pofilelist.

            logIt(String.Format("{0} start: ++ version: {1}",
		        Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName),
		        Process.GetCurrentProcess().MainModule.FileVersionInfo.FileVersion));

            InstallContext myInstallContext = new InstallContext(null, args);
	        if (myInstallContext.IsParameterTrue("debug"))
	        {
		        logIt("Wait any key to dubug, Please attach...\n");
		        Console.ReadKey();
	        }
            if(myInstallContext.IsParameterTrue("help"))
            {
                Usage();
                return 0;
            }

            if(myInstallContext.IsParameterTrue("createconfig"))
            {
                global::ProfileConfig pfconf = new global::ProfileConfig();
                Configs mss = ReserializeMethod();
                foreach(var mm in mss.profiles.Items)
                {
                    global::Profile pofile = new global::Profile();
                    pofile.Name = mm.Name;
                    pofile.Pass = mm.Pass;
                    pofile.Orgsupport = mm.SupportMethod;
                    pofile.Description = mm.Descript;
                    pofile.Bytes = mm.WriteData;
                    pfconf.Profile.Add(pofile);
                }
                String sPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "hdsesconfig.dat");
                using (var output = File.Create(sPath))
                {
                    pfconf.WriteTo(output);
                }
                
                return 0;
            }

            if(myInstallContext.Parameters.ContainsKey("ppid"))
            {
                int parentProcessId = 0;
                if (int.TryParse(myInstallContext.Parameters["ppid"], out parentProcessId))
                {
                    WaitCallback callback = delegate(object processId) { CheckProcess((int)processId); };
                    ThreadPool.QueueUserWorkItem(callback, parentProcessId);
                }
            }

            if(myInstallContext.Parameters.ContainsKey("label"))
            {
                _TAG = String.Format("{0}_{1}", _TAG, myInstallContext.Parameters["label"]);
            }
            int nIndex = 1;
            if (myInstallContext.Parameters.ContainsKey("index"))
            {
                if (!int.TryParse(myInstallContext.Parameters["index"], out nIndex)) return ERRORCODE.ERROR_HDDINDEX;
            }
            else
            {
                Usage();
                return ERRORCODE.ERROR_LESSHDDINDEX;
            }

            String sCmdline = String.Format(@"-y -n 32768 \\.\PhysicalDrive{0} ", nIndex);
            int nPass = 1;
            bool bAddpass = false;
            if (myInstallContext.Parameters.ContainsKey("pass"))
            {
                if(!int.TryParse(myInstallContext.Parameters["pass"], out nPass))
                {
                    nPass = 1;
                }
                else
                {
                    sCmdline = String.Format(@"-y -n 32768 \\.\PhysicalDrive{0} -p {1} ", nIndex, nPass);
                    bAddpass = true;
                }
            }

            Configs ms = ReserializeMethod();    
            
            if(ms==null)
            {
                logIt("need config file.");
                return ERRORCODE.ERROR_CONFIG;
            }
            String sName = "";

            if (myInstallContext.Parameters.ContainsKey("name"))
            {
                sName = myInstallContext.Parameters["name"];
                if (String.Compare("SecureErase", sName, true) == 0)
                {
                    SecurWipe(nIndex);
                    return ERRORCODE.ERROR_SUCCESS;
                }
                foreach (var item in ms.profiles.Items)
                {
                    if(String.Compare(item.Name, sName, true)==0)
                    {
                        if (!bAddpass)
                        {
                            nPass = item.Pass;
                            sCmdline = String.Format(@"-y -n 32768 \\.\PhysicalDrive{0} -p {1} ", nIndex, nPass);
                        }
                        Console.WriteLine(item.Descript);
                        int oldRand =0;
                        if(String.IsNullOrEmpty(item.SupportMethod))
                        {
                            if (!String.IsNullOrEmpty(item.WriteData))
                            {
                                String[] da = item.WriteData.Split(new char[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);
                                StringBuilder sb = new StringBuilder();
                                sb.Append(" ");
                                foreach(var a in da)
                                {
                                    if(string.Compare("*", a, true)==0)
                                    {
                                        Random rnd = new Random();
                                        int rd = rnd.Next(0, 256);
                                        oldRand = rd;
                                        sb.Append(rd).Append(" ");
                                    }
                                    else if (string.Compare("~*", a, true) == 0)
                                    {
                                        int rd = ~oldRand;
                                        byte aard= (byte)(rd & 0xff) ;
                                        sb.Append(aard).Append(" ");
                                    }
                                    else if (string.Compare("C", a, true) == 0)
                                    {
                                        sb.Append("C").Append(" ");
                                    }
                                    else if (string.Compare("R", a, true) == 0)
                                    {
                                        sb.Append("R").Append(" ");
                                    }
                                    else
                                    {
                                        sb.Append(a).Append(" ");
                                    }
                                }
                                sCmdline = String.Format(@"{0} {1}", sCmdline, sb.ToString());
                            }
                            else
                                return ERRORCODE.ERROR_SEQUENCE;
                        }
                        else
                        {
                            String smd = item.SupportMethod;
                            if (item.SupportMethod.StartsWith("-w") || item.SupportMethod.StartsWith("--llformat"))
                            {
                                string ss = smd.Replace("-w", "").Replace("--llformat", "");
                                String[] da = ss.Split(new char[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);
                                StringBuilder sb = new StringBuilder();
                                sb.Append("-w ");
                                foreach (var a in da)
                                {
                                    if (string.Compare("*", a, true) == 0)
                                    {
                                        Random rnd = new Random();
                                        int rd = rnd.Next(0, 256);
                                        sb.Append(rd).Append(";");
                                    }
                                    else
                                    {
                                        try
                                        {
                                            if(a.StartsWith("0x"))
                                            {
                                                sb.Append(Convert.ToInt16(a, 16)).Append(";");
                                            }
                                            else
                                            {
                                                sb.Append(Convert.ToInt16(a)).Append(";");
                                            }
                                        }catch(Exception e)
                                        {
                                            Console.WriteLine(e.ToString());
                                        }
                                    }
                                }
                                smd = sb.ToString();
                            }
                            sCmdline = String.Format(@"{0} {1}", sCmdline, smd);
                        }
                        break;
                    }
                }
            }
            else
            {
                return ERRORCODE.ERROR_CONFIGNAME;
            }
              /// <summary>
            /// The internal process interface used to interface with the process.
            /// </summary>
            ProcessInterface processInterace = new ProcessInterface();
            processInterace.OnProcessOutput += new ProcessEventHanlder(processInterace_OnProcessOutput);
            processInterace.OnProcessError += new ProcessEventHanlder(processInterace_OnProcessError);
            processInterace.OnProcessInput += new ProcessEventHanlder(processInterace_OnProcessInput);
            processInterace.OnProcessExit += new ProcessEventHanlder(processInterace_OnProcessExit);

            String sExeFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "dskwipe.exe");
            if (System.Environment.Is64BitOperatingSystem) sExeFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "dskwipex64.exe");
            if (!File.Exists(sExeFileName)) return ERRORCODE.ERROR_EXENOEXIST;
            logIt(String.Format("Run command line:{0}", sCmdline));
            processInterace.StartProcess(sExeFileName, sCmdline);
            new Thread(()=>{
                while (processInterace.IsProcessRunning && !hangflag.WaitOne(20000))
                {
                    if (waithand.WaitOne(0)) break;
                    processInterace.WriteInput("\x03\r\n");
                }
            });
            JobManagement.Job job = new JobManagement.Job();
            job.AddProcess(processInterace.Process.Handle); 

            waithand.WaitOne();
          

            return ERRORCODE.ERROR_SUCCESS;
        }

        private static void processInterace_OnProcessExit(object sender, ProcessEventArgs args)
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("WipeExitCode="+args.Code);
            Console.BackgroundColor = ConsoleColor.Black;
            waithand.Set();
        }

        private static void processInterace_OnProcessInput(object sender, ProcessEventArgs args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(args.Content);  
        }

        private static void processInterace_OnProcessError(object sender, ProcessEventArgs args)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine(args.Content);
            hangflag.Set();
        }

        private static void processInterace_OnProcessOutput(object sender, ProcessEventArgs args)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine(args.Content);
            hangflag.Set();
        }
    }
}
