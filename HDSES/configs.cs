﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HDSES
{
    public class Profile
    {//<profile name="" orgsupport="" bytes="0x12 0x23;0x25;*" pass="1" description="Wipe device using US DoD 5220.22-M method (3 passes)"/>
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("orgsupport")]
        public string SupportMethod { get; set; }
        [XmlAttribute("bytes")]
        public string WriteData { get; set; }
        [XmlAttribute("pass")]
        public int Pass { get; set; }

        [XmlAttribute("description")]
        public string Descript { get; set; }

    }
    public class Profiles
    {
        [XmlElement("profile")]
        public List<Profile> Items = new List<Profile>();
    }
    [XmlRoot("configs")]
    public class Configs
    {
        [XmlElement("profiles")]
        public Profiles profiles = new Profiles();
    }
}
